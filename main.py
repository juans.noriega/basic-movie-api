from fastapi import FastAPI
from config.database import engine, Base
from middlewares.error_handler import ErrorHandler
from routers.movie_router import movie_router
from routers.user_router import user_router

app = FastAPI()
app.title = "My first API with FastAPI"
app.version = "0.0.1"

app.add_middleware(ErrorHandler)
app.include_router(movie_router)
app.include_router(user_router)


Base.metadata.create_all(bind=engine)



movies = [
    {
		"id": 1,
		"title": "Avatar",
		"overview": "En un exuberante planeta llamado Pandora viven los Na'vi, seres que ...",
		"year": 2009,
		"rating": 7.8,
		"category": "Accion"
	},
    {
		"id": 2,
		"title": "El señor de los anillos: La comunidad del anillo",
		"overview": "En la Segunda Edad de la Tierra Media...",
		"year": 2001,
		"rating": 9,
		"category": "Aventuras"
	},
  {
		"id": 3,
		"title": "El señor de los anillos: Las dos torres",
		"overview": "Tras internarse en Emyn Muil ...",
		"year": 2002,
		"rating": 9,
		"category": "Aventuras"
	}
]

@app.get('/', tags=['home'])
def message():
    return "Introducción a FastAPI" 



