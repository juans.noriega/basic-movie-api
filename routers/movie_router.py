from fastapi import Path, Query, Depends, APIRouter
from fastapi.responses import JSONResponse
from typing import List
from config.database import Session
from models.movie import Movie as MovieModel
from fastapi.encoders import jsonable_encoder
from middlewares.jwt_bearer import JWTBearer
from services.movie_services import MovieService
from schemas.movie_schema import Movie

movie_router = APIRouter()

@movie_router.get('/movies', tags=['movies'], response_model=List[Movie], status_code=200, dependencies=[Depends(JWTBearer())])
def get_movies() -> List[Movie]:
    db = Session()
    movies = MovieService(db).get_movies()
    return JSONResponse(status_code=200, content=jsonable_encoder(movies))

@movie_router.get('/movies/{id}', tags=['movies'], response_model=Movie, status_code=200)
def get_movie_by_id(id: int = Path(ge=1, le=2000)) -> Movie: #Path(ge=1, le=2000): Validación parametro de ruta
    db = Session()
    movie = MovieService(db).get_movie(id)
    if movie != None:
        return JSONResponse(status_code=200, content=jsonable_encoder(movie))
    return JSONResponse(status_code=404, content={'message': "No encontrado"})

@movie_router.get('/movies/', tags=['movies'], response_model=List[Movie], status_code=200)
def get_movies_by_category(category: str = Query(min_length=5, max_length=15)) -> List[Movie]: #Query(min_length=5, max_length=15): Validación parametro Query
    db = Session()
    movies = MovieService(db).get_movies_by_category(category)
    return JSONResponse(status_code=200, content=jsonable_encoder(movies))

@movie_router.post('/movies', tags=['movies'], response_model=dict, status_code=201)
def create_movie(movie: Movie) -> dict:
    db = Session() #Para iniciar la base de datos
    MovieService(db).create_movie(movie)
    return JSONResponse( status_code=201, content={"message": "Se ha registrado la película"})

@movie_router.put('/movies/{id}', tags=['movies'], response_model=dict, status_code=200)
def update_movie(id: int , movie: Movie) -> dict:
  db = Session()
  result = MovieService(db).get_movie(id)
  if not result:
    return JSONResponse(status_code=404, content={'message': "No encontrado"})
  MovieService(db).update_movie(id, movie)
  return JSONResponse(status_code=200, content={"message": "Se ha modificado la película"})

@movie_router.delete('/movies/{id}', tags=['movies'], response_model=dict, status_code=200)
def delete_movie(id: int = Path(ge=1, le=2000)) -> dict:
    db = Session()
    movie = MovieService(db).get_movie(id)
    if not movie:
        return JSONResponse(status_code=404, content={'message': "No encontrado"})
    MovieService(db).delete_movie(id)
    return JSONResponse(status_code=200, content={"message": "Se ha eliminado la película"})