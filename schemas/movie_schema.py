from pydantic import BaseModel, Field
from typing import Optional

#Con Field se esta manejando la validación de datos. La clase Config esta haciendo un esquema de como deberia crearse el servicio para mostrar en la documentación
class Movie(BaseModel):
    id: Optional[int] = None
    title: str = Field(min_length=5, max_length=200)
    overview: str = Field(min_length=15, max_length=500)
    year: int = Field(le=2022)
    rating:float = Field(ge=1, le=10) # ge: Mayor que en ingles. le: Menor que en ingles
    category:str = Field(min_length=5, max_length=15)

    class Config:
        schema_extra = {
            "example": {
                "id": 1,
                "title": "Mi película",
                "overview": "Descripción de la película",
                "year": 2022,
                "rating": 9.8,
                "category" : "Acción"
            }
        }